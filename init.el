(require 'package)

;; execute path from shell
;;(when (memq window-system '(mac ns x))
;;  (exec-path-from-shell-initialize))

;; Melpa
(add-to-list 'package-archives
       '("melpa" . "http://melpa.org/packages/") t)

(add-to-list 'package-archives
         '("melpa-stable" . "http://stable.melpa.org/packages/"))

(package-initialize)
(when (not package-archive-contents)
  (package-refresh-contents))


(defvar myPackages
  '(better-defaults
    ein
    elpy
    flycheck
    material-theme
    py-autopep8
    projectile
    company-jedi
    helm-projectile
    auto-complete
    epc
    jedi
    ido-vertical-mode
    fill-column-indicator
    flx-ido
    ivy
    magit
    material-theme
    dracula-theme
    zenburn-theme
    aggressive-indent
    auto-yasnippet))


;; projectile
;;(projectile-global-mode)
;;(setq projectile-completion-system 'helm)
;;(helm-projectile-on)


(mapc #'(lambda (package)
    (unless (package-installed-p package)
      (package-install package)))
      myPackages)

;;---------------------------------------
;; PYHON CONFIG
;;---------------------------------------
;; enable elpy
(elpy-enable)
;;(elpy-use-ipython)

;; use flycheck not flymake with elpy
(when (require 'flycheck nil t)
  (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
  (add-hook 'elpy-mode-hook 'flycheck-mode))

;; enable autopep8 formatting on save
(require 'py-autopep8)
(add-hook 'elpy-mode-hook 'py-autopep8-enable-on-save)


(require 'projectile)
(define-key projectile-mode-map (kbd "s-p") 'projectile-command-map)
(define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
(projectile-mode +1)
(projectile-global-mode)
(setq projectile-project-search-path '("~/projects/"))
;; Sort modification time
(setq projectile-sort-order 'modification-time)
;; projectilee cahing
(setq projectile-enable-caching t)
;; projectile root evrwhere
(setq projectile-require-project-root -1)

;; Auto complete
(require 'auto-complete-config)
(ac-config-default)
(setq ac-show-menu-immediately-on-auto-complete t)


;; jedi
(require 'jedi)
(setq jedi:setup-keys t)
(setq jedi:complete-on-dot t)
(add-hook 'python-mode-hook 'jedi:setup)

(setq jedi-custom-file (expand-file-name "jedi-custom.el" user-emacs-directory))
(when (file-exists-p jedi-custom-file)
  (load jedi-custom-file))

;; Hook up to autocomplete
(add-to-list 'ac-sources ' as-source-jedi-direct)
;;Enable for pyhton mode
(add-hook 'python-mode-hook 'jedi:setup)

;;; Jedi server setup  - C-h v jedi:server-args
;;(defvar jedi-config:with-virtalenv nil
;;  "Set to non-nil to point to a particular virtualenv." )
;;
;;;;Variables to help find the project root
;;(defvar jedi-config:vcs-root-sentinel ".git")
;;(defvar jedi-config:python-module-sentinel "__init__.py")
;;
;;;; Function to find project root given a buffer
;;(defun get-project-root (buf repo-type init-file)
;; (vc-find-root
;;   (expand-file-name
;;    (buffer-file-name buf)) repo-type))

;; EPC
(require 'epc)

;; Magit
(require 'magit)
(global-set-key (kbd "C-x g") 'magit-status)
;;ido in magit copleteion
(setq magit-completing-read-function 'magit-ido-completing-read)

;;-----------------------------------------------
;;CUSTOMIZATION  - Look and feel
;;------------------------------------------------

;;language
(setq current-language-environment "English")

;; hide the startup message
(setq inhibit-startup-message t) 

;; Theme
(load-theme 'material t) ;; load material theme
;;(load-theme 'dracula t)

;; enable line numbers globally
(global-linum-mode t) 

;; buffer visible bell
(setq visible-bell t)


;; Highlight the line we are currently on
(global-hl-line-mode t)

;; turn on highlight matching brackets when cursor is on one
(show-paren-mode t)

;; reload files automatically when changed on disk
(global-auto-revert-mode t)

;; Inden with spaces
(setq indent-tabs-mode -1)


;; ignore case when searching
(setq case-fold-search t)

;; require final newlines in files when they are saved
(setq require-final-newline t)


;;don't show the startup screen
(setq inhibit-startup-screen t)
;;don't show the menu bar
;;(menu-bar-mode -1)

(require 'tool-bar)
(tool-bar-mode -1)
; don't show the scroll bar
(scroll-bar-mode -1)

; number of characters until the fill column
(setq fill-column 70)

;;;;
;buffer-name completion for C-x b
;;(iswitchb-mode 1)  ;; Obsolete

(require 'fringe)
(fringe-mode 10)

; lines which are exactly as wide as the window (not counting the
; final newline character) are not continued. Instead, when point is
; at the end of the line, the cursor appears in the right fringe.
(setq overflow-newline-into-fringe t)

; each line of text gets one line on the screen (i.e., text will run
; off the left instead of wrapping around onto a new line)
(setq truncate-lines t)
; truncate lines even in partial-width windows
(setq truncate-partial-width-windows t)

; display line numbers to the right of the window
(global-linum-mode t)
; show the current line and column numbers in the stats bar as well
(line-number-mode t)
(column-number-mode t)

;; Agressive indent mode
(add-hook 'emacs-lisp-mode-hook #'aggressive-indent-mode)
(add-hook 'css-mode-hook #'aggressive-indent-mode) 
(global-aggressive-indent-mode 1)
(add-to-list 'aggressive-indent-excluded-modes 'html-mode)
;; for python -- takes care of auto disabling
(setq aggressive-indent-excluded-modes
      (remove 'python-mode aggressive-indent-excluded-modes)) 

;;Yasnippet
(add-to-list 'load-path
              "~/.emacs.d/plugins/yasnippet")
(require 'yasnippet)
(yas-global-mode 1)

;; Cursor ;;;
; make sure transient mark mode is enabled (it should be by default,
; but just in case)
(transient-mark-mode t)

; turn on mouse wheel support for scrolling
(require 'mwheel)
(mouse-wheel-mode t)

;;; Syntax Highlighting ;;;;
;; text decoration
(require 'font-lock)
(setq font-lock-maximum-decoration t)
(global-font-lock-mode t)
(global-hi-lock-mode -1)
(setq jit-lock-contextually t)
(setq jit-lock-stealth-verbose t)

; if there is size information associated with text, change the text
; size to reflect it
(size-indication-mode t)

; highlight parentheses when the cursor is next to them
(require 'paren)
(show-paren-mode t)
(setq show-paren-delay 0)
(setq show-paren-style 'expression)
(set-face-background 'show-paren-match "#b4b4b4")
(set-face-foreground 'show-paren-match "#000000")
(set-face-attribute 'show-paren-match nil :weight 'extra-bold)

;;;; increase decreese text size
(global-set-key (kbd "C-+") 'text-scale-increase)
(global-set-key (kbd "C--") 'text-scale-decrease)
;; C-x C-0 restores the default font size

;;; Auto fill mode
(defun auto-fill-mode-on () (auto-fill-mode 1))
(add-hook 'text-mode-hook 'auto-fill-mode-on)
(add-hook 'emacs-lisp-mode 'auto-fill-mode-on)
(add-hook 'tex-mode-hook 'auto-fill-mode-on)
(add-hook 'latex-mode-hook 'auto-fill-mode-on)

;; Remove trailing white space upon saving
;; Note: because of a bug in EIN we only delete trailing whitespace
;; when not in EIN mode.
(add-hook 'before-save-hook
          (lambda ()
            (when (not (derived-mode-p 'ein:notebook-multilang-mode))
              (delete-trailing-whitespace))))

;; fill column indicator
(add-to-list 'load-path "~/.emacs.d/fill-column-indicator-1.83")
(require 'fill-column-indicator)
(define-globalized-minor-mode
 global-fci-mode fci-mode (lambda () (fci-mode 1)))
(global-fci-mode t)

;;windows super key
;;(setq w32-pass-lwindow-to-system nil)
;;(setq w32-lwindow-modifier 'super) ; Left Windows key

;;  changing for visisble buffers
(global-set-key (kbd "C-x <up>") 'windmove-up)
(global-set-key (kbd "C-x <down>") 'windmove-down)
(global-set-key (kbd "C-x <left>") 'windmove-left)
(global-set-key (kbd "C-x <right>") 'windmove-right)

;; Changing for minibuffer
(defun switch-to-minibuffer-window ()
    "switch to minibuffer window (if active)"
    (interactive)
    (when (active-minibuffer-window)
        (select-window (active-minibuffer-window))))
(global-set-key (kbd "s-x") 'switch-to-minibuffer-window)


; window modifications
;;(global-set-key (kbd "s-c-") 'shrink-window-horizontally)
;;(global-set-key (kbd "s-c-") 'enlarge-window-horizontally)
;;(global-set-key (kbd "s-c-") 'shrink-window)
;;(global-set-key (kbd "s-c-") 'enlarge-window)




;; Ido mode
(require 'ido)
(setq ido-enable-prefix -1
      ido-enable-flex-matching t
      ido-case-fold -1
      ido-auto-merge-work-directories-length -1
      ido-create-new-buffer 'always
      ido-use-filename-at-point -1
      ido-max-prospects 10
      ido-everywhere t)

;; flx-ido
(require 'flx-ido)
(ido-mode 1)
(ido-everywhere 1)
(flx-ido-mode 1)
;; disable ido faces to see flx highlights.
(setq ido-enable-flex-matching t)
(setq ido-use-faces -1)

(require 'ido-vertical-mode)

(setq ido-vertical-define-keys 'C-n-and-C-p-only)
(setq ido-use-faces t)
(set-face-attribute 'ido-vertical-first-match-face nil
                    :background "#e5b7c0")
(set-face-attribute 'ido-vertical-only-match-face nil
                    :background "#e52b50"
                    :foreground "white")
(set-face-attribute 'ido-vertical-match-face nil
                    :foreground "#b00000")
(ido-vertical-mode 1)

(ido-mode 1)


;; frame toggle

(defun toggle-frame-split ()
  "If the frame is split vertically, split it horizontally or vice versa.
Assumes that the frame is only split into two."
  (interactive)
  (unless (= (length (window-list)) 2) (error "Can only toggle a frame split in two"))
  (let ((split-vertically-p (window-combined-p)))
    (delete-window) ; closes current window
    (if split-vertically-p
        (split-window-horizontally)
      (split-window-vertically)) ; gives us a split with the other window twice
    (switch-to-buffer nil))) ; restore the original window in this part of the frame

;; I don't use the default binding of 'C-x 5', so use toggle-frame-split instead
(global-set-key (kbd "C-x |") 'toggle-frame-split)


;;; window split toggle
(defun toggle-window-split ()
  (interactive)
  (if (= (count-windows) 2)
      (let* ((this-win-buffer (window-buffer))
	     (next-win-buffer (window-buffer (next-window)))
	     (this-win-edges (window-edges (selected-window)))
	     (next-win-edges (window-edges (next-window)))
	     (this-win-2nd (not (and (<= (car this-win-edges)
					 (car next-win-edges))
				     (<= (cadr this-win-edges)
					 (cadr next-win-edges)))))
	     (splitter
	      (if (= (car this-win-edges)
		     (car (window-edges (next-window))))
		  'split-window-horizontally
		'split-window-vertically)))
	(delete-other-windows)
	(let ((first-win (selected-window)))
	  (funcall splitter)
	  (if this-win-2nd (other-window 1))
	  (set-window-buffer (selected-window) this-win-buffer)
	  (set-window-buffer (next-window) next-win-buffer)
	  (select-window first-win)
	  (if this-win-2nd (other-window 1))))))
(define-key ctl-x-4-map "t" 'toggle-window-split)

;; Python shell in line emacs bug fix
(with-eval-after-load 'python
  (defun python-shell-completion-native-try ()
    "Return non-nil if can trigger native completion."
    (let ((python-shell-completion-native-enable t)
          (python-shell-completion-native-output-timeout
           python-shell-completion-native-try-output-timeout))
      (python-shell-completion-native-get-completions
       (get-buffer-process (current-buffer))
       nil "_"))))
(setq python-shell-completion-native-enable nil)